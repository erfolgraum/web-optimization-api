function getTemplateItem(title, desc, height, color, bgColor, bgImage) {
  const template = document.createElement("template");
  template.innerHTML = `
    <style>
        .app-section {
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            padding: 87px 30px 100px;
            background: linear-gradient(${bgColor}), url(${bgImage}) no-repeat center/cover;
            height: ${height};
            max-width: 100%;
            color: ${color};
        }   
    
        .app-logo {
            width: 64px;
            height: 64px;
            object-fit: contain;
        }
    
        .app-title {
            font-size: 3.25rem;
            line-height: 3.20rem;
            text-align: center;
            font-family: 'Oswald', sans-serif;
        }
    
        app-title--black {
            color: black
        }
    
        .app-subtitle {
            font-size: 1.50rem;
            line-height: 1.625rem;
            font-weight: 300;
            text-align: center;
            font-family: 'Source Sans Pro', sans-serif, Arial;
        }

        
    </style>
    
    <section class="app-section">    
    <slot name="head-section"></slot>    
        <h1 class="app-title">
            ${title}
        </h1>
        
        <slot name="more"></slot>
        <h2 class="app-subtitle">
            ${desc}
        </h2>
        <slot name="content"></slot>
    </section>
    `;

  return template;
}

// Create a new Custom Element (new class inherits from HTMLElement)
class WebsiteSection extends HTMLElement {
  constructor() {
    super();

    this.attachShadow({ mode: "open" });
    const getTemplateAttribute = getTemplateItem(
      this.getAttribute("title"),
      this.getAttribute("desc"),
      this.getAttribute("height"),
      this.getAttribute("color"),
      this.getAttribute("bgColor"),
      this.getAttribute("bgImage")
    );

    this.shadowRoot.appendChild(getTemplateAttribute.content.cloneNode(true));
    // this.shadowRoot.querySelector(".app-logo").src = this.getAttribute("logo");
    // this.shadowRoot.querySelector(".app-section--image-peak").src = this.getAttribute("wallpaper");

    const fontOswald = document.createElement("link");
    fontOswald.href =
      "https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap";
    fontOswald.rel = "stylesheet";
    document.head.appendChild(fontOswald);
  }
}

/* Register your Custom Element (define and register the website-section
     Web Component as Custom Element in the global window) */
window.customElements.define("website-section", WebsiteSection);
