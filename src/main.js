import { creator, email, form, btn } from "./join-us-section.js";
import { storeToLocalStorage } from "./store-to-localstorage.js";
import "./styles/style.css";
import { getUsers } from "./ajax.js";
import "./web-component.js";
import { durationFetching } from "./performance-api.js"



document.addEventListener("DOMContentLoaded", function () {
  // creator.create('advanced')
  creator.create("standard");
  // creator.create().remove()

  form.addEventListener("input", (e) => {
    e.preventDefault();
    storeToLocalStorage(e.target.value);
  });

  if (localStorage.getItem("email") === null) {
    email.value = "";
    email.style.display = "inline-block";
  }
  if (email.value !== null) {
    email.value = localStorage.getItem("email");
  }

  if (localStorage.getItem("email") && localStorage.getItem("subscribe")) {
    email.style.display = "none";
    btn.setAttribute("value", "Unsubscribe");
  }

  getUsers("/api/community");


 /*Performance API - Define the function for receiving data 
 from the network and measure its execution time*/ 
 const fetchCommunityFromApi = (url) => fetch(url)
 .then((res) => res.json());
 
 durationFetching(fetchCommunityFromApi)("/api/community");

});


 

