export function getUsers(url) {
  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      let output = "";
      data.forEach((user) => {
        console.log(data);
        output += `
        <div class="user">
          <img src=${user.avatar} class="user__img" alt="User${user.id}">
          <p class="user__desc">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolor.
          </p>
          <h4 class="user__name">${user.firstName} ${user.lastName}</h4>
          <div class="user__position">${user.position}</div>
      </div>`;
      });
      document.querySelector(".user__box").innerHTML = output;
    });
}

export function postEmailValue(url, inputValue, callback) {
  try {
    fetch(url, {
      method: "POST",
      headers: {
        Accept: "application/json, text/plain, */*",
        "Content-type": "application/json",
      },
      body: JSON.stringify({ email: inputValue }),
    })
      .then((res) => {
        if (res.status == 422) {
          throw new Error(`HTTP error! Status: ${res.status}`);
        }
        if (res.status === 200) {
          console.log("Subscribe response", res);
          return res.json();
        }
      })
      .catch((err) => {
        console.log("Error after first THEN", err);
        window.alert(`This is an: ${err}`);
      })
      .then((data) => {
        callback();
        console.log(data);
      });
  } catch (err) {
    console.log("Error", err);
  }
}

export function unsubscribeFromCommunity(url, callback2) {
  fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-type": "application/json",
    },
  })
    .then((res) => {
      if (res.ok) {
        console.log("Unsubscribe response", res);
        return res.json();
      }
    })
    .then((data) => {
      callback2();
      console.log("Data", data);
    });
}
