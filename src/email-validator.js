// import { email } from "./join-us-section.js";

export const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

// let regex = new RegExp("[a-z0-9]+@(gmail|outlook|ukr)+\.[a-z]{2,3}$");
export let regex = new RegExp("[a-z0-9]+@(gmail.com|outlook.com|yandex.ru)$");

export let regexEmailEnding = new RegExp("@(gmail.com|outlook.com|yandex.ru)$");


export function validate(email) {
  for (let address of VALID_EMAIL_ENDINGS) {
    if (email.includes(address) && regex.test(email)) {
      return true;
    }
  }
  return false;
}



/* **** Export the validateAsync(email) function ****	
It returns true asynchronously if the email contains a valid ending, 
or false otherwise. Use the Promise API for that.*/

export async function validateAsync(email) { 

  let validEndingPromise =  new Promise((resolve, reject)=> {  
    for (let address of VALID_EMAIL_ENDINGS) {
      if (email.includes(address) && regexEmailEnding.test(email)) {
        resolve(true)
        
      }
    }
      reject(false);
  })

  return validEndingPromise
  .then((data)=> {
    console.log(data) 
    return data 
  })
  .catch((error)=> {
    console.log(error)
    return error
  })
}

// let res = await validateAsync("123@gmail.com")
// console.log("res", res)


/* 
****Export the validateWithThrow(email) function****	
It returns true if the email contains a valid ending. 
Otherwise, it throws an error with a message that 
the provided email is invalid.
*/
export function validateWithThrow(email) {

    for (let address of VALID_EMAIL_ENDINGS) {
      if (email.includes(address) && regexEmailEnding.test(email)) {
        console.log("validateWithThrow() has right email ending")
        return true 
      } else {
        throw new Error(`Email ${email} is not valid!`)
      } 
    }
    
}

// validateWithThrow('123@gmaill.com')


/*
Export the validateWithLog(email) function	
It behaves exactly in the same way as validate(). 
The only addition is that it logs the result to the
 console (using console.log) before returning it.
*/
export function validateWithLog(email) {
  for (let address of VALID_EMAIL_ENDINGS) {
    if (email.includes(address) && regex.test(email)) {
      console.log(`Congrats! Email ${email} is correct`, true)
      return true;
    }
  }
  console.log("Use another email, not", false)
  return false; 
}

const result = validateWithLog('1234@gmaiil.com')
console.log(result)
