// implement a function to measure the execution time of another function

const durationFetching = (func) =>
  async (...args) => {
    const start = performance.now();
    const result = await func(...args);
    console.log(`Result of ${func.name}`, result);
    const difference = Math.round(performance.now() - start);
    console.log(`Duration of executing is: ${difference} ms`);
    //memory
    const memory = (
      Math.round((performance.memory.usedJSHeapSize * 100) / 100) / 1048576
    ).toFixed(2);
    console.log(" Memory usage:", memory, "MB");

    let measureData = {
        fetching: true,
        duration: difference,
        memoryUsage: memory     
    };

    sendMetric("/api/analytics/performance", [measureData])
  };

  
// Send Metric to the server
function sendMetric(url, arr) {
  fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(arr),
  })
    .then((res) => res.json())
    .then((res) => console.log(res));
}

export { durationFetching };
