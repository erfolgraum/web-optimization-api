const path = require("path");
const { merge } = require('webpack-merge')  
const Common = require('./webpack.common.js')
const TerserPlugin = require("terser-webpack-plugin");

module.exports = merge(Common, {

    mode: "production",

    performance: {
      hints: false,
      maxEntrypointSize: 275000,
      maxAssetSize: 275000,
    },

    optimization: {
        minimize: true,
        minimizer: [new TerserPlugin()],
      },
});
